variable "name" {
  description = "This is the name to attach to your resources."
  default     = "test_default"
}

variable "new_vpc_id" {
  description = "VPC ID to assign security groups into"
}

